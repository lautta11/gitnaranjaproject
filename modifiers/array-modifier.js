function arrayModifier (inputArray) {
/* ***
 *
 * Return expected: ['paml', 'rouf', 'moor', 'lalh']
 * 
 *** */ 
    let transformedArr=[]
    for (let str of inputArray){
        transformedArr.push(str.split('').reverse().join(''))
    }
    return transformedArr
}

module.exports = {
    arrayModifier
}