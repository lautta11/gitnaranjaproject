function objectModifier(inputObject) {
/* ***
 *  name: "John",
        lastName: "Doe",
        phone: 1234,
        isEnabled: true
 * Return expected:
 * 
 * {
 *  eman: "name: John",
 *  emaNtsal: "lastName: Doe",
 *  enohp: 5678,
 *  delbanEsi: false
 * }
 * 
*** */
  const modifiedObj = {};
  for (const [key, value] of Object.entries(inputObject)) {
    const modifiedKey = key.split('').reverse().join('');
    modifiedObj[modifiedKey] = value;
  }
return modifiedObj;
}

module.exports = {
  objectModifier,
};
