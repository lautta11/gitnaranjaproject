function stringModifier(inputString) {
/* ***
 * ABDICATORS
 * Return expected: tSrOiDcBaA
 * 
*** */
const sortedString = inputString.split('').sort().reverse();
let outputString = '';

for (let i = 0; i < sortedString.length; i++) {
  if (i % 2 === 0) {
    outputString += sortedString[i].toLowerCase();
  } else {
    outputString += sortedString[i].toUpperCase();
  }
}

return outputString;

}

module.exports = {
  stringModifier,
};
